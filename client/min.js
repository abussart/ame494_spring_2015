function addNew() {
addPrompt.set("Create a new to-do:");
}

var addPrompt = {
value:null,
set: function(question) {
    var output = "";
    output = output + "<p>" + question + "</p>";
    output = output + "<textarea id='add-todo' rows='4' type='text'></textarea>";
    output = output + "<a href='javascript:addPrompt.done()'>Create</a> ";
    output = output + "<a href='javascript:addPrompt.cancel()'>Cancel</a>";
    document.getElementById("container").innerHTML = output;
    document.getElementById("overlay").style.display = "block";
    document.getElementById("container").style.display = "block";
    document.getElementById("add-todo").select();
    var keys = document.getElementById("add-todo");
    keys.addEventListener("keydown", function (e) {
        if (e.keyCode === 13) {
            addPrompt.done();
        }
        if (e.keyCode === 27) {
            addPrompt.cancel();
        }
    });
},
hide: function() {
    document.getElementById("container").innerHTML = "";
    document.getElementById("overlay").style.display = "none";
    document.getElementById("container").style.display = "none";
},
done: function() {
    var content = document.getElementById("container").getElementsByTagName("textarea")[0].value;        
    var newtodo = content;
    if(content.length > 0) {
        var url = "./addtodo?newtodo=" + encodeURIComponent(newtodo) + "&todoid=" + new Date().getTime().toString();
        var callback = function(data){
            if(data === "added"){
                updateList();
            }
        } 
        loadURL(url, callback);
        addPrompt.hide();
    }
    else {
        addPrompt.hide();
        dialogAlert.set("You did not enter any text. To-do cannot be created.");
    }
},
cancel: function() {
    addPrompt.hide();
}

}

var dialogAlert = {
set: function(alert_text){
    var output = "";
    output = output + "<span>" + "Whoops!" + "</span>";
    output = output + "<p>" + alert_text + "</p>";
    output = output + "<a id='confirm-delete' href='javascript:dialogAlert.close()'> Dismiss </a>";
    document.getElementById("container").innerHTML = output;
    document.getElementById("overlay").style.display = "block";
    document.getElementById("container").style.display = "block";
    document.getElementById("confirm-delete").focus();    
    var keys = document.getElementById("confirm-delete");
    keys.addEventListener("keydown", function (e) {
        if (e.keyCode === 13) {
            dialogAlert.close();
        }
        if (e.keyCode === 27) {
            dialogAlert.close();
        }
    });
    document.getElementById("overlay").onclick = function() {
        dialogAlert.close();
    }
},
close: function(){		
    document.getElementById("container").innerHTML = "";
    document.getElementById("overlay").style.display = "none";
    document.getElementById("container").style.display = "none";
},
}


function deleteTodo(index) {
confirmDelete.set(index, "Are you sure you want to delete this todo?");
}

var confirmDelete = {
value: null,
index: null,
set: function(index, question){
    var output = "";
    output = output + "<p>" + question + "</p>";
    output = output + "<a id='confirm-delete' href='javascript:confirmDelete.done(" + index + ")'> Delete </a>";
    output = output + "<a href='javascript:confirmDelete.cancel()'> Cancel </a>";
    document.getElementById("container").innerHTML = output;
    document.getElementById("overlay").style.display = "block";
    document.getElementById("container").style.display = "block";
    document.getElementById("confirm-delete").focus();

},
hide: function(){		
    document.getElementById("container").innerHTML = "";
    document.getElementById("overlay").style.display = "none";
    document.getElementById("container").style.display = "none";
},
done: function(index){
    confirmDelete.value = true;
    confirmDelete.hide();
    var url = "./deletetodo?index=" + encodeURIComponent(index);
    var callback = function(data){
        if(data === "deleted"){
            updateList();
        }
    } 
    loadURL(url, callback);         
},
cancel: function(){
    confirmDelete.value = false;
    confirmDelete.hide();
}
}


function editTodo(id, str) {
editPrompt.set("Edit your todo:", getTodoText(id), id);
}

var editPrompt = {
value:null,
set: function(question, todotext, id) {
    var output = "";
    output = output + "<p>" + question + "</p>";
    output = output + "<textarea id='edit-todo' rows='4' type='text'>" + todotext + "</textarea>";
    output = output + "<a href='javascript:editPrompt.done(" + id + ")'>Confirm</a> ";
    output = output + "<a href='javascript:editPrompt.cancel()'>Cancel</a>";
    document.getElementById("container").innerHTML = output;
    document.getElementById("overlay").style.display = "block";
    document.getElementById("container").style.display = "block";
    document.getElementById("edit-todo").select();
    var enter = document.getElementById("edit-todo");
    enter.addEventListener("keydown", function (e) {
        if (e.keyCode === 13) {
            editPrompt.done(id);
        }
        if (e.keyCode === 27) {
            editPrompt.cancel();
        }
    });
},
hide: function() {
    document.getElementById("container").innerHTML = "";
    document.getElementById("overlay").style.display = "none";
    document.getElementById("container").style.display = "none";
},
done: function(id) {
    var content = document.getElementById("container").getElementsByTagName("textarea")[0].value;        
    var newtodo = content;     
    if(content.length > 0) {
        var url = "./edittodo?todoid=" + encodeURIComponent(id) + "&newtodo=" + encodeURIComponent(newtodo);
        var callback = function(data){
            if(data === "done"){
                updateList();
            }
        } 
        loadURL(url, callback); 
        editPrompt.hide();
    }
    else {
        editPrompt.hide();
        dialogAlert.set("You erased the contents of this to-do. To-do cannot be saved.");
    }
},
cancel: function() {
    editPrompt.hide();
}
}

function getTodoText(id) {
for(var i = 0; i < allTodos.length; i++){
    if( parseInt(allTodos[i].todoid) == id){
        return allTodos[i].newtodo;
    }
}
return "";
}


var allTodos;
function updateList() {
var url = "./listtodos";	 
var callback = function(data){
    var list = JSON.parse(data);
    allTodos = list;
    var output = "";
    if(list.length > 0) {
        for(var i=0; i < list.length; i++){
            output = output + "<div class='todo-item'>";
            output = output + "<p>" + list[i].newtodo + "</p>";
            output = output + "<div class='todo-buttons'>";
            output = output + "<a href='javascript:editTodo(" + list[i].todoid.toString() + ")'>" + "<i class='fa fa-pencil'></i></a>";
            output = output + "<a href='javascript:deleteTodo(" + list[i].todoid.toString() + ")'>" + "<i class='fa fa-minus'></i></a>";
            output = output + "</div>";
            output = output + "<hr>";
            output = output + "</div>";
        }
        document.getElementById("list").innerHTML = output + "<div onclick='javascript:clearAll()' id='clear' title='Clear all To-dos'><i class='fa fa-trash-o'></i></div>";
    }        
    else {
        document.getElementById("list").innerHTML = "You do not have any to-dos. Click the <i class='fa fa-plus'></i></a> to add a to-do to the list.";
    }        
}  
loadURL(url, callback);
}

function clearAll() {
confirmClear.set("This will erase all to-dos. Are you sure?");
}

var confirmClear = {
value: null,
set: function(question){
    var output = "";
    output = output + "<p>" + question + "</p>";
    output = output + "<a id='confirm-delete' href='javascript:confirmClear.done()'> Delete </a>";
    output = output + "<a href='javascript:confirmClear.cancel()'> Cancel </a>";
    document.getElementById("container").innerHTML = output;
    document.getElementById("overlay").style.display = "block";
    document.getElementById("container").style.display = "block";
    document.getElementById("confirm-delete").focus();

},
hide: function(){		
    document.getElementById("container").innerHTML = "";
    document.getElementById("overlay").style.display = "none";
    document.getElementById("container").style.display = "none";
},
done: function(){
    confirmClear.value = true;
    confirmClear.hide();
    var url = "./clearall";
    var callback = function(data){
        if(data === "cleared"){
            updateList();
        }
    } 
    loadURL(url, callback);         
},
cancel: function(){
    confirmClear.value = false;
    confirmClear.hide();
}
}



//jquery
$(document).ready(function(){
var $add = $("#add");
var $add_small = $("#add_small");
var pos = $add.position();                    
$(window).scroll(function() {
    var windowpos = $(window).scrollTop();
    if (windowpos >= pos.top) {
        $add_small.slideDown("fast");
    } else {
        $add_small.slideUp("fast");
    }
});
});

$(document).ready(function(){
$("#helpFile").load("help.html");
});

$(document).ready(function(){
$("#help").click(function() {
    $("#helpFile").fadeIn();
    $("#overlay").fadeIn();
});
$("#overlay").click(function() {
    $("#helpFile").fadeOut();
    $("#overlay").fadeOut();
    $("#container").fadeOut();
});

});